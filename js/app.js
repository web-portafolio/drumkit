let audio;

//Funcione de sonido a cada boton
function buttonsOnKeypad(key) {
  switch (key) {
    case "w":
      audio = new Audio("sounds/crash.mp3");
      break;
    case "a":
      audio = new Audio("sounds/kick-bass.mp3");
      break;
    case "s":
      audio = new Audio("sounds/snare.mp3");
      break;
    case "d":
      audio = new Audio("sounds/tom-1.mp3");
      break;
    case "j":
      audio = new Audio("sounds/tom-2.mp3");
      break;
    case "k":
      audio = new Audio("sounds/tom-2.mp3");
      break;
    case "l":
      audio = new Audio("sounds/tom-4.mp3");
      break;
  }
  audio.play();
}

//Funcione de animacion a cada boton
function buttonAnimation(currentKey) {
  let activateAnimation = document.querySelector("." + currentKey);
  activateAnimation.classList.add("pressed");
  setTimeout(function () {
    activateAnimation.classList.remove("pressed");
  }, 100);
}

//Asignación de funciones a cada boton
document.addEventListener("keypress", function(event){
  buttonsOnKeypad(event.key);
  buttonAnimation(event.key);
})
